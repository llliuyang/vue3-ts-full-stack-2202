## 1.element-plus 组件 api 挂载到 app

添加文件 src/plugins/element.ts

src/plugins/element.ts 代码内容：

```typescript
import { App } from "vue";
import {
  ElButton,
  ElMessage,
  ElNotification,
  ElMessageBox,
} from "element-plus";
import ElementPlus from "element-plus";
import "element-plus/dist/index.css";
// Element Plus 组件内部默认使用英语
// https://element-plus.gitee.io/zh-CN/guide/i18n.html
import zhCn from "element-plus/es/locale/lang/zh-cn";
// Element Plus 直接使用了 Day.js 项目的时间日期国际化设置, 并且会自动全局设置已经导入的 Day.js 国际化配置。
import "dayjs/locale/zh-cn";

// $ELEMENT size属性类型
export type Size = "default" | "medium" | "small" | "mini";

export default (app: App): void => {
  app.use(ElementPlus, {
    locale: zhCn,
  });
  // 按需导入组件列表
  const components = [ElButton, ElMessage, ElNotification, ElMessageBox];

  components.forEach((component) => {
    app.use(component);
  });

  // Vue.prototype 替换为 config.globalProperties
  // 文档说明 https://v3.cn.vuejs.org/guide/migration/global-api.html#vue-prototype-%E6%9B%BF%E6%8D%A2%E4%B8%BA-config-globalproperties
  app.config.globalProperties.$message = ElMessage;
  app.config.globalProperties.$notify = ElNotification;
  app.config.globalProperties.$confirm = ElMessageBox.confirm;
  app.config.globalProperties.$alert = ElMessageBox.alert;
  app.config.globalProperties.$prompt = ElMessageBox.prompt;

  // element-plus全局配置
  // 说明文档：https://element-plus.gitee.io/#/zh-CN/component/quickstart#quan-ju-pei-zhi
  // 该对象目前支持 size 与 zIndex 字段。size 用于改变组件的默认尺寸 small，zIndex 设置弹框的初始 z-index（默认值：2000）。
  app.config.globalProperties.$ELEMENT = {
    size: "medium",
    // zIndex: 2000 弹框zIndex默认值：2000
  };
};
```

##

> 安装 dayjs npm install dayjs

##

## 2.类型声明问题

src/main.ts

```typescript
import { createApp } from "vue";
import router from "./router/index";
import store from "./store";
import App from "./App.vue";
import "normalize.css/normalize.css";
import "@/styles/index.scss";
import "virtual:svg-icons-register";
import initSvgIcon from "@/icons/index";
// element-plus
import installElementPlus from "./plugins/element";

createApp(App)
  .use(store)
  .use(installElementPlus)
  .use(initSvgIcon)
  .use(router)
  .mount("#app");
```

> 安装@vue/runtime-core

```shell
npm install @vue/runtime-core
```

创建自定义声明文件 src/runtime.d.ts

![输入图片说明](image/1.5.1.png)

```typescript
// 挂载到vue实例上
import { ElMessageBox, ElMessage, ElNotification } from "element-plus";
import { Size } from "./plugins/element";

// vue实例上挂载属性类型声明
declare module "@vue/runtime-core" {
  interface ComponentCustomProperties {
    $message: typeof ElMessage;
    $notify: typeof ElNotification;
    $confirm: typeof ElMessageBox.confirm;
    $alert: typeof ElMessageBox.alert;
    $prompt: typeof ElMessageBox.prompt;
    $ELEMENT: {
      size: Size;
    };
  }
}
```

**_ 自定义类型声明不能放在 env.d.ts 里，会导致.vue 文件不能识别。_**

## 3.组件中使用

src/views/dashborad/index.vue

![输入图片说明](image/1.5.2.png)

当敲写 proxy.$时 挂载类型成功提示出来了

![输入图片说明](image/1.5.3.png)

```vue
<template>
  <div>
    <h1>Dashboard page</h1>
    <svg-icon icon-class="bug"></svg-icon>
    <!-- icon-class svg图标名称 class-name 额外的自定义类名 @click绑定事件 -->
    <svg-icon
      icon-class="404"
      class-name="custom-class"
      @click="sayHi"
    ></svg-icon>
  </div>
</template>

<script setup lang="ts">
import { getCurrentInstance } from "vue";
const { proxy } = getCurrentInstance()!;

const sayHi = () => {
  proxy?.$message.success("恭喜你，这是一条成功消息");
};
</script>
<style lang="scss">
.custom-class {
  // 自定义样式404
  font-size: 200px;
  color: green;
}
</style>
```

点击 404 图标 效果图

![输入图片说明](image/1.5.4.png)

## eslintrc.js

rules 关闭了一条规则

```javascript
{
  "rules": {
    "no-unused-expressions": "off"
  }
}
```

## 本节源码参考

[https://gitee.com/zhufengpeixun/vue3-admin2](https://gitee.com/zhufengpeixun/vue3-admin2)

对于每节文章有问题需要补充评论的 大家可以写在每节下方评论处 感谢
