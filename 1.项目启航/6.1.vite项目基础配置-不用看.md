## 1.element-plus 按需导入

### Vite

参考文档：[ElementPlus 文档](https://element-plus.gitee.io/#/zh-CN/component/quickstart)
首先，安装 vite-plugin-style-import:

```bash
$ npm install vite-plugin-style-import -D
或者
$ yarn add vite-plugin-style-import -D
```

然后，将 vite.config.js 修改为：

- 针对 sass 引入.scss 样式
  > 确保已经安装 npm i sass -D

```typescript
import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import styleImport from "vite-plugin-style-import";

export default defineConfig({
  plugins: [
    vue(),
    styleImport({
      libs: [
        {
          libraryName: "element-plus",
          esModule: true,
          ensureStyleFile: true,
          resolveStyle: (name) => {
            name = name.slice(3);
            return `element-plus/packages/theme-chalk/src/${name}.scss`;
          },
          resolveComponent: (name) => {
            return `element-plus/lib/${name}`;
          },
        },
      ],
    }),
  ],
});
```

#### 按需导入组件

在 src 目录下创建 plugins 目录并创建 element.ts

![输入图片说明](image/1.6.1.png)

element.ts 内容

> 组件按需导入，都在这里导入

```typescript
import { App } from "vue";
import { locale, ElButton } from "element-plus";
// import 'element-plus/packages/theme-chalk/src/base.scss'
// Element Plus 组件内部默认使用英语
// https://element-plus.gitee.io/#/zh-CN/component/i18n
import lang from "element-plus/lib/locale/lang/zh-cn";
// Element Plus 直接使用了 Day.js 项目的时间日期国际化设置, 并且会自动全局设置已经导入的 Day.js 国际化配置。
import "dayjs/locale/zh-cn";

export default (app: App) => {
  locale(lang);

  // 组件列表
  const components = [ElButton];

  components.forEach((component) => {
    app.component(component.name, component);
  });

  // 全局配置
  // 该对象目前支持 size 与 zIndex 字段。size 用于改变组件的默认尺寸 small，zIndex 设置弹框的初始 z-index（默认值：2000）。
  app.config.globalProperties.$ELEMENT = {
    size: "medium",
  };
};
```

在入口 main.ts vue 插件形式 use element.ts

```typescript
import { createApp } from "vue";
import ElementPlus from "./plugins/element";
import App from "./App.vue";

const app = createApp(App);

// plugins
app.use(ElementPlus);

app.mount("#app");
```

## 2.初始化 css

安装 normalize.css

```bash
npm install --save normalize.css
```

main.ts 中导入

![输入图片说明](image/1.6.2.png)
