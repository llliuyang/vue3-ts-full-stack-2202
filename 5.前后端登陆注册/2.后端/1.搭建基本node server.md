## 1.创建 vue3-admin-server 文件夹

```bash
cd vue3-admin-server
npm init -y
```

## 2.安装依赖

```bash
npm i @koa/cors @koa/router koa koa-json koa-bodyparser koa-logger
```

安装 ts 相关包

```bash
npm i -D @types/koa @types/koa-bodyparser @types/koa-json @types/koa-logger @types/koa__cors @types/koa__router typescript
```

安装 node 工具

```bash
npm i ts-node nodemon -D
```

![输入图片说明](image/5.2.1.1.png)

## 3.创建入口文件

src/app.ts

```typescript
import Koa from "koa";
import cors from "@koa/cors";
import logger from "koa-logger";
import bodyparser from "koa-bodyparser";
// routes
import authRoutes from "./routes/auth";

// koa应用实例
const app = new Koa();

// middlewares
app.use(cors()); // 支持跨域
app.use(
  bodyparser({
    // 解析请求体
    enableTypes: ["json", "form", "text"],
  })
);
app.use(logger()); // 开发日志中间件

// routes
// 用户验证路由（登录 注册）
app.use(authRoutes.routes()).use(authRoutes.allowedMethods());

// listen
const port = process.env.PORT || "3003";
app.listen(port, () => {
  console.log(`server listening on ${port}`);
});

app.on("error", (err) => console.error("server error", err));
```

## 4.创建 routes 管理路由

> src/routes 是专门来存放 router

src/routes/auth.ts

```typescript
import Router from "@koa/router";

const router = new Router({
  prefix: "/auth",
});

router.get("/test", async (ctx) => {
  ctx.body = "auth test router";
});

export default router;
```

## 5.配置 npm script

```json
{
  ...
  "scripts": {
    "serve": "nodemon -i ./node_modules/ --exec \"ts-node\" src/app.ts"
  }
}
```

## 6.运行

```bash
npm run serve
```

效果图

![输入图片说明](image/5.2.1.2.png)

## 本节参考源码

[https://gitee.com/brolly/vue3-admin-server/commit/826495934b7cef9ab044359b8da261092409f6ed](https://gitee.com/brolly/vue3-admin-server/commit/826495934b7cef9ab044359b8da261092409f6ed)
