![输入图片说明](image/6.1.3.1.png)

![输入图片说明](image/6.1.3.2.png)

![输入图片说明](image/6.1.3.3.png)
菜单权限分配设计到 菜单表和角色表关联

![输入图片说明](image/6.1.3.4.png)

![输入图片说明](image/6.1.3.5.png)
## 3-1 角色路由表

src/routes/roles.ts

```typescript
import Router from "@koa/router";
import {
  addRoleController,
  getAllRoleController,
  updateRoleController,
  removeRoleController,
} from "../controller/roles";

const router = new Router({
  prefix: "/api/role",
});

/**
 * 添加角色
 * post /api/role
 */

router.post("/", async (ctx) => {
  ctx.body = await addRoleController(ctx.request.body);
});

/**
 * 获取全部角色
 * get /api/role
 */
router.get("/", async (ctx) => {
  const { pageNum = 0, pageSize = 10 } = ctx.request.query;
  ctx.body = await getAllRoleController({
    offset: Number(pageNum),
    limit: Number(pageSize),
  });
});

/**
 * 编辑角色
 * put /api/role/:id
 */
router.put("/:id", async (ctx) => {
  const { id } = ctx.params;
  ctx.body = await updateRoleController(Number(id), ctx.request.body);
});

/**
 * 删除角色
 * delete /api/role/:id
 */
router.delete("/:id", async (ctx) => {
  const { id } = ctx.params;
  ctx.body = await removeRoleController(Number(id));
});

export default router;
```

## 3-2 角色 controller

src/controller/roles.ts

```typescript
import { RoleModelProps } from "../db/models/roles";
import {
  createRole,
  getRole,
  getAllRoleService,
  updateRoleById,
  removeRoleById,
} from "../services/roles";
import { createErrorResponse, SuccessResponse } from "../utils/Response";
import errorInfo from "../constants/errorInfo";

const {
  addAccessFailInfo,
  addRoleNameExistInfo,
  updateRoleFailInfo,
  updateRoleNameExistInfo,
  removeRoleFailInfo,
} = errorInfo;

// 添加菜单
export const addRoleController = async (params: RoleModelProps) => {
  const result = await getRole(params.name);
  if (result) {
    return createErrorResponse(addRoleNameExistInfo);
  }
  if (params) {
    try {
      const result = await createRole({
        ...params,
      });
      return new SuccessResponse(result);
    } catch (error) {
      console.error(error.message);
      return createErrorResponse(addAccessFailInfo);
    }
  }
};

// 获取全部菜单
interface RoleListParams {
  offset: number;
  limit: number;
}

export const getAllRoleController = async ({
  offset,
  limit,
}: RoleListParams) => {
  try {
    const result = await getAllRoleService(offset, limit);
    return new SuccessResponse(result);
  } catch (error) {
    console.error(error.message);
    return createErrorResponse(addAccessFailInfo);
  }
};

// 编辑角色
export const updateRoleController = async (
  id: number,
  data: RoleModelProps
) => {
  const result = await getRole(data.name || "");
  if (result && result.id !== id) {
    return createErrorResponse(updateRoleNameExistInfo);
  }
  try {
    await updateRoleById(id, data);
    return new SuccessResponse(null, "角色编辑成功!");
  } catch (error) {
    console.error(error.message);
    return createErrorResponse(updateRoleFailInfo);
  }
};

// 删除角色
export const removeRoleController = async (id: number) => {
  try {
    await removeRoleById(id);
    return new SuccessResponse(null, "删除成功!");
  } catch (error) {
    console.error(error.message);
    return createErrorResponse(removeRoleFailInfo);
  }
};
```

## 3-3 角色 service

src/services/roles.ts

```typescript
import RoleModel, { RoleModelProps } from "../db/models/roles";

// 创建菜单资源
export const createRole = async (params: RoleModelProps) => {
  const result = await RoleModel.create({
    ...params,
  });
  return result.toJSON();
};

// 根据角色名称获取角色
export const getRole = async (name: string) => {
  const result = await RoleModel.findOne({
    where: {
      name,
    },
  });
  if (result == null) return null;
  return result.toJSON() as RoleModelProps;
};

// 获取全部角色
export const getAllRoleService = async (offset = 0, limit = 10) => {
  const { count, rows } = await RoleModel.findAndCountAll({
    limit,
    offset: limit * offset,
  });
  return {
    roles: rows,
    count,
  };
};

// 编辑角色
export const updateRoleById = async (id: number, data: RoleModelProps) => {
  const { name, description, is_default } = data;
  const result = await RoleModel.update(
    {
      name,
      description,
      is_default,
    },
    {
      where: {
        id,
      },
    }
  );
  return result;
};

// 删除角色
export const removeRoleById = async (id: number) => {
  const result = await RoleModel.destroy({
    where: {
      id,
    },
  });

  return result;
};
```

## 3-4 角色 model

src/db/models/roles.ts

```typescript
import { Model, DataTypes, Optional } from "sequelize";
import seq from "../seq";

export interface RoleModelProps {
  id: number;
  name: string;
  description: string;
  is_default: number;
}

interface RoleCreationAttributes
  extends Optional<RoleModelProps, "id" | "is_default"> {}

interface RoleInstance
  extends Model<RoleModelProps, RoleCreationAttributes>,
    RoleModelProps {}

const Role = seq.define<RoleInstance>("Role", {
  id: {
    primaryKey: true,
    type: DataTypes.INTEGER.UNSIGNED,
    autoIncrement: true,
  },
  name: {
    type: DataTypes.STRING,
    allowNull: false,
    // unique: true,
    comment: "角色名称 唯一",
  },
  description: {
    type: DataTypes.TEXT,
    comment: "说明描述",
  },
  is_default: {
    type: DataTypes.BOOLEAN,
    defaultValue: 0,
    comment: "默认角色 1是 0不是",
  },
});

export default Role;
```

导入到 models/index
db/models/index.ts

```typescript
import UserModel from "./user";
import AccessModel from "./access";
import RolesModel from "./roles";

export { UserModel, AccessModel, RolesModel };
```

同步 model

```bash
npm run db
```

## 目前最新参考源码

[https://gitee.com/brolly/vue3-admin-server](https://gitee.com/brolly/vue3-admin-server)
